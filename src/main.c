/* BLP file importer plugin for the GIMP
 * Copyright (C) 2009 Coy Don Barnes <coy.barnes@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "config.h"

#include <string.h>

#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>

#include "main.h"
#include "blp.h"
#include "plugin-intl.h"

/* Global varables */
const gchar *filename = NULL;
gboolean interactive = FALSE;
gboolean lastvals = FALSE;

/* Local function prototypes */
static void query (void);
static void run (const gchar *name,
                gint nparams,
                const GimpParam *param,
                gint *nreturn_vals,
                GimpParam **return_vals);


GimpPlugInInfo PLUG_IN_INFO =
{
    NULL,   /* init_proc */
    NULL,   /* quit_proc */
    query,  /* query_proc */
    run,    /* run_proc */
};

MAIN ()

static void
query (void)
{
  static const GimpParamDef load_args[] =
  {
    {
     GIMP_PDB_INT32,
     "run-mode",
     "Interactive, non-interactive"
    },
    {
     GIMP_PDB_STRING,
     "filename",
     "The name of the file to load"
    },
    {
     GIMP_PDB_STRING,
     "raw-filename",
     "The name entered"
    },
  };
  static const GimpParamDef load_return_vals[] =
  {
      {
       GIMP_PDB_IMAGE,
       "image",
       "Output image"
      },
  };
#if 0
  static const GimpParamDef save_args[] =
  {
      {
       GIMP_PDB_INT32,
       "run-mode",
       "Interactive, non-interactive"
      },
      {
       GIMP_PDB_IMAGE,
       "image",
       "Input image"
      },
      {
       GIMP_PDB_DRAWABLE,
       "drawable",
       "Drawable to save"
      },
      {
       GIMP_PDB_STRING,
       "filename",
       "The name of the file to save the image in"
      },
      {
       GIMP_PDB_STRING,
       "raw-filename",
       "The name entered"
      },
  };
#endif

  gimp_install_procedure ("file-blp-load",
                          "Loads files of Blizzard BLP file format",
                          "Loads files of Blizzard BLP file format",
                          "Coy Don Barnes",
                          "Coy Don Barnes",
                          "2009",
                           N_ ("Blizzard BLP image"),
                           NULL,
                           GIMP_PLUGIN,
                           G_N_ELEMENTS (load_args),
                           G_N_ELEMENTS (load_return_vals),
                           load_args, load_return_vals);

  gimp_register_file_handler_mime ("file-blp-load", "image/blp");
  gimp_register_magic_load_handler ("file-blp-load", "blp",
                                    "", "0,string,BLP");
#if 0
  gimp_install_procedure ("file-blp-save",
                          "Saves files in Blizzard BLP file format",
                          "Saves files in Blizzard BLP file format",
                          "Coy Don Barnes",
                          "Coy Don Barnes",
                          "2009",
                          N_ ("Blizzard BLP image"),
                          "INDEXED, GRAY, RGB*",
                          GIMP_PLUGIN,
                          G_N_ELEMENTS (save_args), 0,
                          save_args, NULL);

  gimp_register_file_handler_mime ("file-blp-save", "image/blp");
  gimp_register_save_handler ("file-blp-save", "blp", "");
#endif
}


static void
run (const gchar *name,
    gint nparams,
    const GimpParam *param,
    gint *nreturn_vals,
    GimpParam **return_vals)
{
  static GimpParam values[2];
  GimpRunMode run_mode;
  GimpPDBStatusType status = GIMP_PDB_SUCCESS;
  gint32 image_ID;
  //gint32 drawable_ID;
  //GimpExportReturn export = GIMP_EXPORT_CANCEL;
  GError *error = NULL;

  INIT_I18N();

  run_mode = param[0].data.d_int32;

  *nreturn_vals = 1;
  *return_vals = values;
  values[0].type = GIMP_PDB_STATUS;
  values[0].data.d_status = GIMP_PDB_EXECUTION_ERROR;

  if (strcmp (name, "file-blp-load") == 0)
    {
      switch (run_mode)
        {
          case GIMP_RUN_INTERACTIVE:
            interactive = TRUE;
            break;

          case GIMP_RUN_NONINTERACTIVE:
            /* Make sure all the arguments are there! */
            if (nparams != 3)
              status = GIMP_PDB_CALLING_ERROR;
            break;

          default:
            break;
        }

      if (status == GIMP_PDB_SUCCESS)
        {
          image_ID = ReadBLP (param[1].data.d_string, &error);

          if (image_ID != -1)
            {
              *nreturn_vals = 2;
              values[1].type = GIMP_PDB_IMAGE;
              values[1].data.d_image = image_ID;
            }
          else
            status = GIMP_PDB_EXECUTION_ERROR;
        }
    }
#if 0
  else if (strcmp (name, "file-blp-save") == 0)
    {
      image_ID = param[1].data.d_int32;
      drawable_ID = param[2].data.d_int32;

      /* eventually export the image */
      switch (run_mode)
        {
          case GIMP_RUN_INTERACTIVE:
            interactive = TRUE;
            /* fallthrough */

          case GIMP_RUN_WITH_LAST_VALS:
            if (run_mode == GIMP_RUN_WITH_LAST_VALS)
              lastvals = TRUE;
            gimp_ui_init ("file-blp", FALSE);
            export = gimp_export_image (&image_ID,
                     &drawable_ID, "BLP",
                     (GIMP_EXPORT_CAN_HANDLE_RGB
                      | GIMP_EXPORT_CAN_HANDLE_ALPHA
                      | GIMP_EXPORT_CAN_HANDLE_GRAY
                      | GIMP_EXPORT_CAN_HANDLE_INDEXED));

              if (export == GIMP_EXPORT_CANCEL)
                {
                  values[0].data.d_status = GIMP_PDB_CANCEL;
                  return;
                }
              break;

          case GIMP_RUN_NONINTERACTIVE:
            /* Make sure all the arguments are there! */
            if (nparams != 5)
              status = GIMP_PDB_CALLING_ERROR;
            break;

          default:
            break;
        }

      if (status == GIMP_PDB_SUCCESS)
        {
          status = WriteBMP (param[3].data.d_string, image_ID,
                      drawable_ID, &error);
        }

      if (export == GIMP_EXPORT_EXPORT)
          gimp_image_delete (image_ID);
    }
#endif
  else
    status = GIMP_PDB_CALLING_ERROR;

  if (status != GIMP_PDB_SUCCESS && error)
    {
      *nreturn_vals = 2;
      values[1].type = GIMP_PDB_STRING;
      values[1].data.d_string = error->message;
    }

  values[0].data.d_status = status;
}
