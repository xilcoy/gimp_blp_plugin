/* BLP file importer Plug-in for the GIMP
 * Copyright (C) 2009 Coy Don Barnes <coy.barnes@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "config.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <glib/gstdio.h>

#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>

#include "main.h"
#include "blp.h"
#include "plugin-intl.h"

typedef guint16 rgb565;
#define RGB565_R_MASK (0xf800)
#define RGB565_R_OFF (11)
#define RGB565_G_MASK (0x07e0)
#define RGB565_G_OFF (5)
#define RGB565_B_MASK (0x001f)
#define RGB565_B_OFF (0)

typedef guint32 rgba32;
#define RGBA32_R_MASK (0x000000ff)
#define RGBA32_R_OFF (0)
#define RGBA32_G_MASK (0x0000ff00)
#define RGBA32_G_OFF (8)
#define RGBA32_B_MASK (0x00ff0000)
#define RGBA32_B_OFF (16)
#define RGBA32_A_MASK (0xff000000)
#define RGBA32_A_OFF (24)


struct blp1_header
{
  gint8 magic[3];
  gint8 version;
  guint32 type;
  guint32 flags;
  guint32 width;  /* Must be a power of 2 */
  guint32 height; /* Must be a power of 2 */
  guint32 pic_type;
  guint32 pic_subtype;
  guint32 offsets[16];
  guint32 sizes[16];
  /* does not exist in all circumstances
   * union rgbacolor8 pallete[256];
   */
};

#define BLP1_TYPE_COMPRESSED 0
#define BLP1_TYPE_UNCOMPRESSED 0
#define BLP1_FLAGS_ALPHA 0x08

struct blp2_header
{
  gint8 magic[3];
  gint8 version;
  guint32 type;
  guint8 encoding;
  guint8 alpha_depth;
  guint8 alpha_encoding;
  guint8 hasmips;
  guint32 width;  /* Must be a power of 2 */
  guint32 height; /* Must be a power of 2 */
  guint32 offsets[16];
  guint32 sizes[16];
  guint8 pallete[256 * 4];  /* Always exists */
};

#define BLP2_TYPE_COMPRESSED 0
#define BLP2_TYPE_UNCOMPRESSED 1

#define BLP2_ENCODING_UNCOMPRESSED 1
#define BLP2_ENCODING_DIRECTX_COMPRESSED 2

#define BLP2_ALPHA_ENC_DXT1 0 /* (0 or 1 bit alpha) */
#define BLP2_ALPHA_ENC_DXT3 1 /* (4 bit alpha) */
#define BLP2_ALPHA_ENC_DXT5 7 /* (interpolated alpha) */

#define BLP2_MIPMAP_NONE 0
#define BLP2_MIPMAP_PRESENT 1

#define BLP_FMT_RAW 0
#define BLP_FMT_RAW_ALPHA 1
#define BLP_FMT_RAW_ALPHA_8 2
#define BLP_FMT_DXT1 3
#define BLP_FMT_DXT1_ALPHA 4
#define BLP_FMT_DXT3 5
#define BLP_FMT_DXT5 6


static void
rgb565_to_GimpRGB (GimpRGB *c1, const rgb565 *c2)
{
  gimp_rgba_set_uchar (c1,
                       ((*c2 & RGB565_R_MASK) >> RGB565_R_OFF) * 8,
                       ((*c2 & RGB565_G_MASK) >> RGB565_G_OFF) * 4,
                       ((*c2 & RGB565_B_MASK) >> RGB565_B_OFF) * 8,
                       0xff);
}


static void
blp2_hdr_le_to_cpu (struct blp2_header *hdr)
{
  gint i;

  hdr->width = GUINT32_FROM_LE (hdr->width);
  hdr->height = GUINT32_FROM_LE (hdr->height);
  for (i = 0; i < 16; ++i)
    {
      hdr->offsets[i] = GUINT32_FROM_LE (hdr->offsets[i]);
      hdr->sizes[i] = GUINT32_FROM_LE (hdr->sizes[i]);
    }

  for (i = 0; i < 256; ++i)
    hdr->pallete[i] = GUINT32_FROM_LE (hdr->pallete[i]);
}


static gint
blp2_dxt_texels (struct blp2_header *hdr)
{
  gint texels = 0;
  gint i, div;

  for (i = 0; i < 16; ++i)
    {
      div = 4 << i;

      if (hdr->offsets[i] == 0 || hdr->sizes[i] == 0)
        break;

      texels += MAX (1, hdr->height / div) * MAX (1, hdr->width / div);
    }

  return texels;
}


static void
dxt5_alpha_decode (guint8 *alpha, guint8 *buf)
{
  alpha[0] = buf[0];
  alpha[1] = buf[1];
  if (alpha[0] > alpha[1])
    {
      alpha[2] = (6 * alpha[0] + 1 * alpha[1]) / 7;
      alpha[3] = (5 * alpha[0] + 2 * alpha[1]) / 7;
      alpha[4] = (4 * alpha[0] + 3 * alpha[1]) / 7;
      alpha[5] = (3 * alpha[0] + 4 * alpha[1]) / 7;
      alpha[6] = (2 * alpha[0] + 5 * alpha[1]) / 7;
      alpha[7] = (1 * alpha[0] + 6 * alpha[1]) / 7;
    }
  else
    {
      alpha[2] = (4 * alpha[0] + 1 * alpha[1]) / 5;
      alpha[3] = (3 * alpha[0] + 2 * alpha[1]) / 5;
      alpha[4] = (2 * alpha[0] + 3 * alpha[1]) / 5;
      alpha[5] = (1 * alpha[0] + 4 * alpha[1]) / 5;
      alpha[6] = 0;
      alpha[7] = 255;
    }
}


static void
dxt_rgb_decode (GimpRGB *color, guint8 *buf, gboolean alpha)
{
  rgb565 col[2];

  col[0]  = buf[0] + (buf[1] * 256);
  rgb565_to_GimpRGB (&color[0], &col[0]);

  col[1]  = buf[2] + (buf[3] * 256);
  rgb565_to_GimpRGB (&color[1], &col[1]);

  if (col[0] > col[1])
    {
      /* 2/3 c0 + 1/3 c1 */
      color[2] = color[0];
      gimp_rgb_multiply (&color[2], 2.0);
      gimp_rgb_add (&color[2], &color[1]);
      gimp_rgb_multiply (&color[2], (double)1.0 / 3);

      /* 1/3 c0 + 2/3 c1 */
      color[3] = color[1];
      gimp_rgb_multiply (&color[3], 2.0);
      gimp_rgb_add (&color[3], &color[2]);
      gimp_rgb_multiply (&color[3], (double)1.0 / 3);
    }
  else
    {
      /* 1/2 c0 + 1/2 c1 */
      color[2] = color[0];
      gimp_rgb_add (&color[2], &color[1]);
      gimp_rgb_multiply (&color[2], 0.5);

      /* Transparent black */
      gimp_rgba_set (&color[3], 0.0, 0.0, 0.0, alpha ? 0.0 : 1.0);
    }
}


static gboolean
read_dxt_texel (GimpDrawable *drawable, guint8 *buf, GError **error,
               gint x, gint y, gint w, gint h, gint type)
{
  GimpRGB c[4];
  GimpPixelRgn pixel_rgn;
  guint64 alpha;
  gint i, j, idx;
  gint rgb_off;
  gboolean rgb_alpha;
  guint8 a[8];
  guint8 texel[16 * 4];
  guint8 *p;

  switch (type)
    {
      case BLP_FMT_DXT1:
        rgb_alpha = FALSE;
        rgb_off = 0;
        buf += 8 * (y * w / 4 + x);
        break;
  
      case BLP_FMT_DXT1_ALPHA:
        rgb_alpha = TRUE;
        rgb_off = 0;
        buf += 8 * (y * w / 4 + x);
        break;
  
      case BLP_FMT_DXT3:
        rgb_alpha = FALSE;
        rgb_off = 8;
        buf += 16 * (y * w / 4 + x);
        break;
      case BLP_FMT_DXT5:
        buf += 16 * (y * w / 4 + x);
        dxt5_alpha_decode (a, &buf[0]);
        alpha = ((guint64)buf[2] << 0) + ((guint64)buf[3] << 8)
                 + ((guint64)buf[4] << 16) + ((guint64)buf[5] << 24)
                 + ((guint64)buf[6] << 32) + ((guint64)buf[7] << 40);
        rgb_alpha = FALSE;
        rgb_off = 8;
        break;
      default:
        return FALSE;
    }
  
  dxt_rgb_decode (c, &buf[rgb_off], rgb_alpha);
  
  p = texel;
  for (i = 0; i < 4; ++i)
    {
      for (j = 0; j < 4; ++j)
        {
          if (i < h && j < w)
            {
              idx = buf[rgb_off + 4 + i] >> (j * 2) & 0x3;
              if (type != BLP_FMT_DXT1_ALPHA)
                {
                  gimp_rgb_get_uchar (&c[idx], &p[0], &p[1], &p[2]);
                  p += 3;
                }
              else
                {
                  gimp_rgba_get_uchar (&c[idx], &p[0], &p[1], &p[2], &p[3]);
                  p += 4;
                }

              if (type == BLP_FMT_DXT3)
                *p++ = ((buf[ i * 2 + j / 2] >> (j % 2 ? 4 : 0)) & 0xf) * 17;
              else if (type == BLP_FMT_DXT5)
                *p++ = a[(alpha >> (4 * 3 * i + 3 * j)) & 7];
          }
       }
    }
  gimp_pixel_rgn_init (&pixel_rgn, drawable, x * 4, y * 4,
                       MIN (w, 4), MIN (h, 4), TRUE, FALSE);
  gimp_pixel_rgn_set_rect (&pixel_rgn, texel, x * 4, y * 4,
                           MIN (w, 4), MIN (h, 4));
  gimp_drawable_flush (drawable);

  return TRUE;
}


static gint32
ReadDXTImage (FILE *fd, GError **error, struct blp2_header *hdr,
             GimpImageBaseType base_type, GimpImageType type,
             gint blp_fmt)
{
  GimpDrawable *drawable;
  gint i, j, mip, width, height, div;
  gint cur_progress, max_progress;
  gint32 image, layer;
  guint8 *buf;
  gchar layer_name[64];

  image = gimp_image_new (hdr->width, hdr->height, base_type);

  cur_progress = 0;
  max_progress = blp2_dxt_texels (hdr);

  for (mip  = 0; mip < 16; ++mip)
    {
      div = 1 << mip;

      if (hdr->offsets[mip] == 0 || hdr->sizes[mip] == 0)
        break;

      buf = g_malloc (hdr->sizes[mip]);
      fseek (fd, hdr->offsets[mip], SEEK_SET);
      if (fread (buf, 1, hdr->sizes[mip], fd) != hdr->sizes[mip])
        {
          g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
                       _("Unexpected end-of-file reading: %s'"),
                      gimp_filename_to_utf8 (filename));
          g_free (buf);
          gimp_image_delete (image);
          return -1;
        }

      width = MAX (1, hdr->width / div);
      height = MAX (1, hdr->height / div);
      snprintf (layer_name, 64, "%dx%d", width, height);
      layer = gimp_layer_new (image, layer_name, width, height, type, 100,
                              GIMP_NORMAL_MODE);

      gimp_image_add_layer (image, layer, mip);
      drawable = gimp_drawable_get (layer);

      for (i = 0; i < MAX (height / 4, 1); ++i)
        {
          for (j = 0; j < MAX (width / 4, 1); ++j)
            {
              if (!read_dxt_texel (drawable, buf, error,
                                   j, i, width, height,
                                   blp_fmt))
                {
                  /* Error! */
                  g_free (buf);
                  gimp_image_delete (image);
                  return -1;
                }
              ++cur_progress;
              gimp_progress_update ((gdouble)cur_progress / (gdouble)max_progress);
            }
        }

      if (mip == 0)
        gimp_drawable_set_visible (layer, TRUE);
      else
        gimp_drawable_set_visible (layer, FALSE);
      gimp_drawable_detach (drawable);
    }

  return image;
}


static gint32
ReadRawImage (FILE *fd, GError **error, struct blp2_header *hdr,
              GimpImageBaseType base_type, GimpImageType type,
              gint blp_fmt)
{
  guchar *cmap;
  GimpDrawable *drawable;
  GimpPixelRgn pixel_rgn;
  gint i, j, idx, mip, width, height, div;
  gint cur_progress, max_progress;
  gint32 image, layer;
  guint8 *buf, *abuf, *row, *p;
  gchar layer_name[64];

  image = gimp_image_new (hdr->width, hdr->height, base_type);

  cmap = g_malloc (256 * 3);
  p = hdr->pallete;
  for (i = 0; i < 256 * 3; i += 3)
    {
      cmap[i + 0] = p[2];
      cmap[i + 1] = p[1];
      cmap[i + 2] = p[0];
      p += 4;
    }

  gimp_image_set_colormap (image, cmap, 256);
  g_free (cmap);

  cur_progress = 0;
  max_progress = 0;
  for (mip  = 0; mip < 16; ++mip)
    {
      if (hdr->offsets[mip] == 0 || hdr->sizes[mip] == 0)
        break;

      max_progress += hdr->sizes[mip];
    }

  for (mip  = 0; mip < 16; ++mip)
    {
      div = 1 << mip;

      if (hdr->offsets[mip] == 0 || hdr->sizes[mip] == 0)
        break;

      width = MAX (1, hdr->width / div);
      height = MAX (1, hdr->height / div);
      snprintf (layer_name, 64, "%dx%d", width, height);
      layer = gimp_layer_new (image, layer_name, width, height, type, 100,
                              GIMP_NORMAL_MODE);

      gimp_image_add_layer (image, layer, mip);
      drawable = gimp_drawable_get (layer);
      buf = g_malloc (hdr->sizes[mip]);
      fseek (fd, hdr->offsets[mip], SEEK_SET);
      if (fread (buf, 1, hdr->sizes[mip], fd) != hdr->sizes[mip])
        {
          g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
                       _("Unexpected end-of-file reading: %s'"),
                      gimp_filename_to_utf8 (filename));
          g_free (buf);
          gimp_image_delete (image);
          return -1;
        }
      abuf = buf + hdr->width * hdr->height;

      row = g_malloc (width * 2);
      for (i = 0; i < height; ++i)
        {
          p = row;
          for (j = 0; j < width; ++j)
            {
              idx = i * width + j;
              *p++ = buf[idx];
              if (blp_fmt == BLP_FMT_RAW_ALPHA)
                {
                  idx = (i * width + j) / 8;
                  *p++ = (abuf[idx] & (1 << (j % 8))) ? 0xFF : 0x00;
                }
              else if (blp_fmt == BLP_FMT_RAW_ALPHA_8)
                *p++ = (abuf[idx]);
              ++cur_progress;
            }
          gimp_pixel_rgn_init (&pixel_rgn, drawable, 0, i,
                               width, 1, TRUE, FALSE);
          gimp_pixel_rgn_set_rect (&pixel_rgn, row, 0, i,
                                   width, 1);
          gimp_drawable_flush (drawable);

          gimp_progress_update ((gdouble)cur_progress / (gdouble)max_progress);
        }

      g_free (row);

      if (mip == 0)
        gimp_drawable_set_visible (layer, TRUE);
      else
        gimp_drawable_set_visible (layer, FALSE);

      gimp_drawable_detach (drawable);

      g_free (buf);
    }

  return image;
}


static gint32
ReadBLP1 (FILE *fd, GError **error)
{
  g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
               _("BLP 1 is unsupported"));
  return -1;
#if 0
  struct blp1_header *hdr;

  hdr = malloc (sizeof (*hdr));
  if (hdr == NULL)
    {
      fprintf (stderr, "Out of memory. Cannot allocate blp1 header\n");
      return;
    }
  fseek (fd, 0, SEEK_SET);
  fread (hdr, sizeof (*hdr), 1, fd);
  printf ("BLP version 1\n");
  free (hdr);
#endif
}


static gint32
ReadBLP2 (FILE *fd, GError **error)
{
  struct blp2_header *hdr;
  GimpImageBaseType base_type;
  GimpImageType type;
  gint32 image;
  gint blp_fmt;

  hdr = g_new (struct blp2_header, 1);

  fseek (fd, 0, SEEK_SET);
  if (fread (hdr, sizeof (*hdr), 1, fd) != 1)
   {
      g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_FAILED, "%s",
                   _("Unrecognized or invalid BLP header."));
      g_free (hdr);
      return -1;
   }

  blp2_hdr_le_to_cpu (hdr);

  if (hdr->type == BLP2_TYPE_UNCOMPRESSED
      && hdr->encoding == BLP2_ENCODING_UNCOMPRESSED
      && hdr->alpha_depth == 0)
    {
      base_type = GIMP_INDEXED;
      type = GIMP_INDEXED_IMAGE;
      blp_fmt = BLP_FMT_RAW;
    }
  else if (hdr->type == BLP2_TYPE_UNCOMPRESSED
           && hdr->encoding == BLP2_ENCODING_UNCOMPRESSED
           && hdr->alpha_depth == 1)
    {
      base_type = GIMP_INDEXED;
      type = GIMP_INDEXEDA_IMAGE;
      blp_fmt = BLP_FMT_RAW_ALPHA;
    }
  else if (hdr->type == BLP2_TYPE_UNCOMPRESSED
           && hdr->encoding == BLP2_ENCODING_UNCOMPRESSED
           && hdr->alpha_depth == 8)
    {
      base_type = GIMP_INDEXED;
      type = GIMP_INDEXEDA_IMAGE;
      blp_fmt = BLP_FMT_RAW_ALPHA_8;
    }
  else if (hdr->type == BLP2_TYPE_UNCOMPRESSED
           && hdr->encoding == BLP2_ENCODING_DIRECTX_COMPRESSED
           && hdr->alpha_depth == 0)
    {
      base_type = GIMP_RGB;
      type = GIMP_RGB_IMAGE;
      blp_fmt = BLP_FMT_DXT1;
    }
  else if (hdr->type == BLP2_TYPE_UNCOMPRESSED
           && hdr->encoding == BLP2_ENCODING_DIRECTX_COMPRESSED
           && hdr->alpha_depth == 1)
    {
      base_type = GIMP_RGB;
      type = GIMP_RGBA_IMAGE;
      blp_fmt = BLP_FMT_DXT1_ALPHA;
    }
  else if (hdr->type == BLP2_TYPE_UNCOMPRESSED
           && hdr->encoding == BLP2_ENCODING_DIRECTX_COMPRESSED
           && hdr->alpha_depth == 8
           && hdr->alpha_encoding == BLP2_ALPHA_ENC_DXT3)
    {
      base_type = GIMP_RGB;
      type = GIMP_RGBA_IMAGE;
      blp_fmt = BLP_FMT_DXT3;
    }
  else if (hdr->type == BLP2_TYPE_UNCOMPRESSED
          && hdr->encoding == BLP2_ENCODING_DIRECTX_COMPRESSED
          && hdr->alpha_depth == 8
          && hdr->alpha_encoding == BLP2_ALPHA_ENC_DXT5)
    {
      base_type = GIMP_RGB;
      type = GIMP_RGBA_IMAGE;
      blp_fmt = BLP_FMT_DXT5;
    }
  else
    {
      g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_FAILED, "%s",
                   _("Unrecognized or invalid BLP format."));
      printf ("format: %d %d %d %d\n", hdr->type, hdr->encoding, hdr->alpha_depth,
              hdr->alpha_encoding);
      g_free (hdr);
      return -1;
    }

  if (base_type == GIMP_RGB)
    {
      image = ReadDXTImage (fd, error, hdr, base_type, type, blp_fmt);
      if (image >= 0)
        gimp_image_set_filename (image, filename);
    }
  else
    {
      image = ReadRawImage (fd, error, hdr, base_type, type, blp_fmt);
      if (image >= 0)
        gimp_image_set_filename (image, filename);
    }
  g_free (hdr);
  gimp_progress_update (1);
  return image;
}


gint32
ReadBLP (const gchar *name, GError **error)
{
  FILE *fd;
  gchar magic[4];
  gint32 st;

  filename = name;
  fd = g_fopen (filename, "rb");

  if (!fd)
    {
      g_set_error (error, G_FILE_ERROR, g_file_error_from_errno (errno),
                   _("Could not open '%s' for reading: %s"),
                  gimp_filename_to_utf8 (filename), g_strerror (errno));
      fclose (fd);
      return -1;
    }

  gimp_progress_init_printf (_("Opening '%s'"), gimp_filename_to_utf8 (name));

  if (fread (magic, 4, 1, fd) != 1 || (strncmp (magic, "BLP1", 4) != 0
                                       && strncmp (magic, "BLP2", 4) != 0))
    {
      g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
                   _("'%s' is not a valid BLP file"),
                  gimp_filename_to_utf8 (filename));
      fclose (fd);
      return -1;
    }

  if (strncmp (magic, "BLP1", 4) == 0)
    st = ReadBLP1 (fd, error);
  else
    st = ReadBLP2 (fd, error);

  fclose (fd);

  return st;
}
